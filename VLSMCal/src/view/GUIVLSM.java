package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.helpers.ParseConversionEventImpl;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.lang.Math;
import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;


public class GUIVLSM extends JFrame {

	private JPanel contentPane;
	private JTextField tFMsk1;
	private JTextField tFHost;
	private JTextField tFIP1;
	private JTextField tFIP2;
	private JTextField tFIP3;
	private JTextField tFIP4;
	
	private JTable hostsList;
	private JTable resultsList;

	private Color ECDE = new Color(245,169,188); 
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIVLSM frame = new GUIVLSM();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUIVLSM() {
		
		ArrayList<Integer> hosts = new ArrayList();
		ArrayList<Integer> masks = new ArrayList();
		ArrayList<Long> firstIps = new ArrayList();
		ArrayList<Long> lastIps = new ArrayList();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 500);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 204, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 153, 0));
		panel.setBounds(380, 0, 504, 461);
		contentPane.add(panel);
		
		JLabel lblIp = new JLabel("IP");
		lblIp.setFont(new Font("Microsoft Sans Serif", lblIp.getFont().getStyle() & ~Font.BOLD & ~Font.ITALIC, 11));
		lblIp.setBounds(20, 25, 46, 14);
		contentPane.add(lblIp);
		
		tFIP1 = new JTextField();
		tFIP1.setBounds(65, 20, 28, 20);
		contentPane.add(tFIP1);
		tFIP1.setColumns(10);
		
		tFIP2 = new JTextField();
		tFIP2.setBounds(97, 20, 28, 20);
		contentPane.add(tFIP2);
		tFIP2.setColumns(10);
		
		tFIP3 = new JTextField();
		tFIP3.setBounds(129, 20, 28, 20);
		contentPane.add(tFIP3);
		tFIP3.setColumns(10);
		
		tFIP4 = new JTextField();
		tFIP4.setBounds(161, 20, 28, 20);
		contentPane.add(tFIP4);
		tFIP4.setColumns(10);
		
		JLabel point1 = new JLabel(".");
		point1.setBounds(93, 25, 9, 14);
		contentPane.add(point1);
		
		JLabel point2 = new JLabel(".");
		point2.setBounds(125, 25, 9, 14);
		contentPane.add(point2);
		
		JLabel point3 = new JLabel(".");
		point3.setBounds(157, 25, 9, 14);
		contentPane.add(point3);
		
		JLabel lblMask = new JLabel("Mask");
		lblMask.setFont(new Font("Microsoft Sans Serif", lblMask.getFont().getStyle() & ~Font.BOLD & ~Font.ITALIC, 11));
		lblMask.setBounds(20, 56, 46, 14);
		contentPane.add(lblMask);
		
		tFMsk1 = new JTextField();
		tFMsk1.setToolTipText("Write the mask (8-30)");
		tFMsk1.setBounds(63, 53, 28, 20);
		contentPane.add(tFMsk1);
		tFMsk1.setColumns(10);
		
		JLabel lblHosts = new JLabel("Hosts");
		lblHosts.setFont(new Font("Microsoft Sans Serif", lblHosts.getFont().getStyle() & ~Font.BOLD & ~Font.ITALIC, 11));
		lblHosts.setBounds(20, 90, 46, 14);
		contentPane.add(lblHosts);
		
		tFHost = new JTextField();
		tFHost.setBounds(20, 115, 86, 20);
		contentPane.add(tFHost);
		tFHost.setColumns(10);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.setFont(new Font("Microsoft Sans Serif", btnAdd.getFont().getStyle() & ~Font.BOLD & ~Font.ITALIC, 11));
		btnAdd.setBackground(new Color(255, 204, 51));
		btnAdd.setBounds(131, 114, 61, 23);
		contentPane.add(btnAdd);
		
		JButton btnPlay = new JButton("Play");
		btnPlay.setFont(new Font("Microsoft Sans Serif", btnPlay.getFont().getStyle() & ~Font.BOLD & ~Font.ITALIC, 11));
		btnPlay.setBackground(new Color(255, 204, 51));
		btnPlay.setForeground(Color.BLACK);
		btnPlay.setBounds(298, 115, 64, 64);
		contentPane.add(btnPlay);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 146, 256, 280);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(5, 35, 400, 400);
		
		
		hostsList = new JTable();
		hostsList.setModel(new DefaultTableModel(new Object [][]{}, new String[]{"Hosts number by network"}));	
		hostsList.setBounds(10, 108, 150, 170);
		scrollPane.setViewportView(hostsList);	
		DefaultTableModel modelo = (DefaultTableModel) hostsList.getModel();
		contentPane.add(scrollPane);
		
		resultsList = new JTable();
		resultsList.setModel(new DefaultTableModel(new Object[][] {}, new String[] {"Mask", "First ip", "Last ip", "Network Address"}));
		hostsList.setBounds(5, 35, 400, 400);
		scrollPane_1.setViewportView(resultsList);
		DefaultTableModel modelo1 = (DefaultTableModel) resultsList.getModel();
		panel.add(scrollPane_1);
		
		
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				int n=Integer.parseInt(tFHost.getText());
				hosts.add(n);
				
				Object[] row = new Object[1];
				row[0] = n;
				modelo.addRow(row);
			}
			
		});
		
		btnPlay.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(verifyFields() ==5 && !hosts.isEmpty()) {
				int maski = Integer.parseInt(tFMsk1.getText());
				long ipni = Utils.parseIptoLong(tFIP1.getText(), tFIP2.getText(), tFIP3.getText(), tFIP4.getText());
				long iplim = (long) (ipni + Math.pow(2, 32-maski));
				firstIps.add(ipni);
				// TODO Auto-generated method stub
				//Se ordenan todos los hosts
				hosts.sort(null);
				/*Se calcula la cantidad de bits necesarios para cada uno de los hosts usados y se agregan a la lista de m�scaras.
				*/
				for(int i=0; i<hosts.size();i++) {
					for(int j=0; j<32;j++) {
						if(Math.pow(2, j)>=(hosts.get(i)+2)) {
							masks.add(32-j);
							break;
						}
					}
				}
				/*
				 * Calcula todas las ips finales y las iniciales seg�n la m�scara
				 */
				for(int i=hosts.size()-1; i>=0; i--) {
					int mask = masks.get(i);
					ipni+=Math.pow(2, masks.get(i));
					firstIps.add(ipni);
				}
				
			long ip, ip1;
				Object[] data = new Object[4];
				for(int i=0; i<hosts.size();i++) {
					data[0]=masks.get(hosts.size()-1-i);
					ip = firstIps.get(i);
					ip1 = firstIps.get(i+1);
					data[1]=Utils.parseIptoString(ip+1);
					data[2]=Utils.parseIptoString(ip1-1);
					data[3]=Utils.parseIptoString(ip);
					modelo1.addRow(data);
				}
			JOptionPane.showMessageDialog(new JPanel(),"The maximum possible IP is "+Utils.parseIptoString(iplim));				
			}
		}
		});
		
	}
	
	public int verifyFields() {
		int a=-1, b=-1, c=-1, d=-1, mask=-1;
		int m=0;
		String err="";
		try {
			try {
				a=Integer.parseInt(tFIP1.getText().trim());
			} catch (Exception e) {
				tFIP1.setBackground(ECDE);
				// TODO: handle exception
			}
			try {
				b=Integer.parseInt(tFIP2.getText().trim());
			} catch (Exception e) {
				tFIP2.setBackground(ECDE);
				// TODO: handle exception
			}
			try {
				c=Integer.parseInt(tFIP3.getText().trim());
			} catch (Exception e) {
				tFIP3.setBackground(ECDE);
				// TODO: handle exception
			}
			try {
				d=Integer.parseInt(tFIP4.getText().trim());
			} catch (Exception e) {
				tFIP4.setBackground(ECDE);
				// TODO: handle exception
			}
		if(a>=0 && a<256) {
			m++;
		}
		else {
			tFIP1.setBackground(ECDE);
		}
		if(b>=0 && b<256) {
			m++;
		}
		else {
			tFIP2.setBackground(ECDE);
		}
		if(c>=0 && c<256) {
			m++;
		}
		else {
			tFIP3.setBackground(ECDE);
		}
		if(d>=0 && d<256) {
			m++;
		}
		else {
			tFIP4.setBackground(ECDE);
		}
		if(m==4) {
			err+="IP Succesfully converted";
		}
		else {
			err+="Verify the range corresponding to IP";
		}
		}
		catch (Exception e) {
			err+="Check the fields corresponding to IP";
		}
		try {
			mask=Integer.parseInt(tFMsk1.getText());
			if(mask>=8 && mask<33) {
				m++;
				err+="\nMask converted succesfully";
			}
			else {
				tFMsk1.setBackground(ECDE);
				err+="\nCheck the number is between 8 and 33.";
			}
		} catch (Exception e) {
			err+="\n Check the fields corresponding to mask";
			// TODO: handle exception
		}
		
		if(m!=5) {
			JOptionPane.showMessageDialog(new JPanel(),err);	
		}
		return m;
	}
}
