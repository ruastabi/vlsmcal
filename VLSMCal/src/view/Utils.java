package view;

public class Utils {

	public static long parseIptoLong(String firstOctet, String secondOctet, String thirdOctet, String fourthOctet) {
		long ipi;
		ipi=(long) (Long.parseLong(firstOctet)*Math.pow(256, 3))+
				(long) (Long.parseLong(secondOctet)*Math.pow(256, 2))+
				(long) (Long.parseLong(thirdOctet)*256+
						Long.parseLong(fourthOctet));
		System.out.println(ipi);
		return ipi;
		
	}
	
	public static String parseIptoString(long ip) {
		String ipi="";
		ipi+=Long.toString((ip & 4278190080L)/(long)Math.pow(256, 3));
		ipi+=".";
		ipi+=Long.toString((ip & 16711680L)/(long)Math.pow(256, 2));
		ipi+=".";
		ipi+=Long.toString((ip & 65280L)/256);
		ipi+=".";
		ipi+=Long.toString(ip & 255L);
		return ipi;
	}
	
}
